console.log("test ts");

// object
const person: {
    name: string,
    age: number,
    hobbies: string[],
    role: [number, string]
} = {
    name: 'max',
    age: 30,
    hobbies: ['cooking', 'sports'],
    role: [10, 'author']
};

console.log(person);
for (const hobby of person.hobbies) {
    console.log(hobby.toUpperCase());
}

// person.role.push(1); // this bug cannot be caught by ts
console.log(person.role);

// union aliases
type Combinable = number | string;
type ConversionDescriptor = 'as-number' | 'as-text';

function addNum(
    num1: Combinable,
    num2: Combinable,
    resultType: ConversionDescriptor) {
    let result;
    if (typeof num1 === 'number' && typeof num2 === 'number' || resultType === 'as-number') {
        result = +num1 + +num2; // use + to convert to number before adding or fail to compile
    } else {
        result = num1.toString() + num2.toString();
    }

    // if (resultType === 'as-number') {
    //     return parseInt(result);
    // } else {
    //     return result.toString();
    // }
    return result;
}

const combineNumRes = addNum(1, 20, 'as-number');
console.log(combineNumRes);

const combineTextRes = addNum('1', '20', 'as-text');
console.log(combineTextRes);

// Function type
let combineValues: (a: Combinable, b: Combinable, c: ConversionDescriptor) => number | string;
combineValues = addNum;
console.log(combineValues(1, 22, 'as-number'));

/**
 * add the two numbers and then call the callback function where the callback function
 * has to meet the function definition condition here.
 * @param n1
 * @param n2
 * @param cb
 */
function addAndHandle(n1: number, n2: number, cb: (num: number) => void) {
    const result = n1 + n2;
    cb(result);
}

addAndHandle(10, 20, (result) => {
    console.log(result);
});

// never return type means: the function will never return anything
function generateError(message: string, code: number): never {
    throw {message: message, errorCode: code};
}

generateError('An error occurred', 500);

