console.log("multi files test...");
// use '!' to tell tsc that button definitely exist!
const button = document.querySelector('button')!;

button.addEventListener('click', () => {
    console.log('Clicked!');
});

const bindButton = document.querySelector('button')!;

function clickHandler(message: string) {
    console.log('Clicked!' + message);
}

button.addEventListener('click', clickHandler.bind(null, " You're welcome!"));
