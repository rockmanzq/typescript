export {};
let message = 'Welcome back!';
console.log(message);

// Variable Declaration
let x = 10;
const y = 20;

/**
 * let no need to initialize, but const need
 */
let sum;
const title = 'Codevolution';

// Basic Variable Types
let isBeginner: boolean = true;
let total: number = 0;
let name: string = 'Vishwas';

let sentence: string = `My name is ${name}
I am a beginner in TypeScript`;

console.log(sentence);

// Sub types
let n: null = null;
let u: undefined = undefined;

// let isNew: boolean = null;
// let myName: string = null;

// Array type, both ways are okay

let list1: number[] = [1, 2, 3];
let list2: Array<number> = [1, 2, 3];


// Tuple type

let person1: [string, number] = ['Chris', 22];

// Enum type
enum Color {Red = 5, Green= 'Green', Blue = 'BLUE'}
let c: Color = Color.Green;
console.log(c);

// Any type
let randomValue: any = 10;
randomValue = true;
randomValue = 'Vishwas';

/**
 * unknown type: we don't what the user will type in
 * unknown is a bit more restrictive than any, and is better than any
 * user case: if you don't know what use will input, but eventually you know what you
 * want to do with it eventually. Just add an extra check to make sure that what you
 * wanted.
 *
 * why we need Unknown type, since any type
 * don't check when we call non-existed property!
 * but js file will lead to error.
 * But unknown type will check, for example:
 * myUnknownVariable() or myUnknownVariable.toLowerCase() will fail
 */

let myUnknownVariable: unknown = 'ss';
let myVariable: number[] = [1, 2, 3];
// console.log(myVariable.name.firstName);
// myVariable();
let userInput: unknown;
let userName: string;

userInput = 5;
userInput = 'Max';
if (typeof userInput === 'string') { // need to do a type check!
    userName = userInput;
}

/**
 * To check if an object has a name property or not:
 * check type of obj is equal to Object and the name or other property exists
 * in the object. So we are basically making a check to see if the
 * name property exists in the object or not
 * @param obj
 * @output an object which contains the name property as a string
 */
function hasName(obj: any): obj is {name: string} {
    console.log(myVariable);
    console.log(typeof obj === "object");
    console.log("forEach" in obj);
    console.log(obj.name);
    return !!obj &&
        typeof obj === "object" &&
        "forEach" in obj;
}

if (hasName(myVariable)) {
    console.log(myVariable.forEach);
}
/**
 * type assertion typecasting in other languages,
 * we saying that myVariable should be treated as a string and then
 * the toUpperCase method can be applied
 */
myUnknownVariable = (myUnknownVariable as string).toUpperCase();
console.log(myUnknownVariable);

// Type inference
let a;
a = 10;
a = true;

let b = 10;

// Union Types, has intelligence support, which means it can
// infer the property
let multiType: number | boolean;
multiType = 20;
multiType = true;

let anyType: any;
anyType = 20;
anyType = true;

/**
 * Functions, optional (add '?'), default, required parameters
 * @param num1
 * @param num2
 */

function add(num1: number = 10, num2?: number): number {
    if (num2) // num2 can be undefined
        return num1 + num2;
    else
        return num1;
}

add(5, 10);
console.log(add(5));
console.log(add());



/**
 * Interfaces
 * a common use case of optional para here is forms, like registration forms
 * you can use interface, and optional para
 */

interface Person {
    firstName: string;
    lastName?: string;
}

function fullName(person: Person) {
    console.log(person.firstName + ' ' + person.lastName);
}

// not extendable
function fullNameObject(person: {firstName: string, lastName: string}) {
    console.log(person.firstName + ' ' + person.lastName);
}

let p = {
    firstName: 'Bruce',
    lastName: 'Wayne'
};

fullName(p);
fullNameObject(p);

// Classes

class Employee {
    private readonly employeeName: string;

    constructor(name: string) {
        this.employeeName = name;
    }

    greet() {
        console.log('Good morning ' + this.employeeName);
    }

    getEmployeeName(): string {
        return this.employeeName;
    }
}

let emp1 = new Employee('Vishwas');
console.log("\nclasses example: ");
console.log(emp1.getEmployeeName());
emp1.greet();

class Manager extends Employee{
    constructor(managerName: string) {
        super(managerName);
    }
    delegateWork() {
        console.log('Manager delegated tasks' + this.getEmployeeName());
    }
}

let m1 = new Manager('Bruce');
m1.delegateWork();
m1.greet();
console.log(m1.getEmployeeName());

