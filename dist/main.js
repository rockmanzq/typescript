"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let message = 'Welcome back!';
console.log(message);
// Variable Declaration
let x = 10;
const y = 20;
/**
 * let no need to initialize, but const need
 */
let sum;
const title = 'Codevolution';
// Basic Variable Types
let isBeginner = true;
let total = 0;
let name = 'Vishwas';
let sentence = `My name is ${name}
I am a beginner in TypeScript`;
console.log(sentence);
// Sub types
let n = null;
let u = undefined;
// let isNew: boolean = null;
// let myName: string = null;
// Array type, both ways are okay
let list1 = [1, 2, 3];
let list2 = [1, 2, 3];
// Tuple type
let person1 = ['Chris', 22];
// Enum type
var Color;
(function (Color) {
    Color[Color["Red"] = 5] = "Red";
    Color["Green"] = "Green";
    Color["Blue"] = "BLUE";
})(Color || (Color = {}));
let c = Color.Green;
console.log(c);
// Any type
let randomValue = 10;
randomValue = true;
randomValue = 'Vishwas';
/**
 * unknown type: we don't what the user will type in
 * unknown is a bit more restrictive than any, and is better than any
 * user case: if you don't know what use will input, but eventually you know what you
 * want to do with it eventually. Just add an extra check to make sure that what you
 * wanted.
 *
 * why we need Unknown type, since any type
 * don't check when we call non-existed property!
 * but js file will lead to error.
 * But unknown type will check, for example:
 * myUnknownVariable() or myUnknownVariable.toLowerCase() will fail
 */
let myUnknownVariable = 'ss';
let myVariable = [1, 2, 3];
// console.log(myVariable.name.firstName);
// myVariable();
let userInput;
let userName;
userInput = 5;
userInput = 'Max';
if (typeof userInput === 'string') { // need to do a type check!
    userName = userInput;
}
/**
 * To check if an object has a name property or not:
 * check type of obj is equal to Object and the name or other property exists
 * in the object. So we are basically making a check to see if the
 * name property exists in the object or not
 * @param obj
 * @output an object which contains the name property as a string
 */
function hasName(obj) {
    console.log(myVariable);
    console.log(typeof obj === "object");
    console.log("forEach" in obj);
    console.log(obj.name);
    return !!obj &&
        typeof obj === "object" &&
        "forEach" in obj;
}
if (hasName(myVariable)) {
    console.log(myVariable.forEach);
}
/**
 * type assertion typecasting in other languages,
 * we saying that myVariable should be treated as a string and then
 * the toUpperCase method can be applied
 */
myUnknownVariable = myUnknownVariable.toUpperCase();
console.log(myUnknownVariable);
// Type inference
let a;
a = 10;
a = true;
let b = 10;
// Union Types, has intelligence support, which means it can
// infer the property
let multiType;
multiType = 20;
multiType = true;
let anyType;
anyType = 20;
anyType = true;
/**
 * Functions, optional (add '?'), default, required parameters
 * @param num1
 * @param num2
 */
function add(num1 = 10, num2) {
    if (num2) // num2 can be undefined
        return num1 + num2;
    else
        return num1;
}
add(5, 10);
console.log(add(5));
console.log(add());
function fullName(person) {
    console.log(person.firstName + ' ' + person.lastName);
}
// not extendable
function fullNameObject(person) {
    console.log(person.firstName + ' ' + person.lastName);
}
let p = {
    firstName: 'Bruce',
    lastName: 'Wayne'
};
fullName(p);
fullNameObject(p);
// Classes
class Employee {
    constructor(name) {
        this.employeeName = name;
    }
    greet() {
        console.log('Good morning ' + this.employeeName);
    }
    getEmployeeName() {
        return this.employeeName;
    }
}
let emp1 = new Employee('Vishwas');
console.log("\nclasses example: ");
console.log(emp1.getEmployeeName());
emp1.greet();
class Manager extends Employee {
    constructor(managerName) {
        super(managerName);
    }
    delegateWork() {
        console.log('Manager delegated tasks' + this.getEmployeeName());
    }
}
let m1 = new Manager('Bruce');
m1.delegateWork();
m1.greet();
console.log(m1.getEmployeeName());
//# sourceMappingURL=main.js.map